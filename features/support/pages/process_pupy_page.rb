require_relative 'side_menu_panel'

class ProcessPuppyPage
  include PageObject
  include SideMenuPanel

  buttons(:process_puppy, :value => 'Process Puppy')

  def process_first_puppy
    button_element(:value => 'Process Puppy').click
  end

end