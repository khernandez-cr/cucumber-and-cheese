require_relative 'side_menu_panel'

class HomePage
  include PageObject
  include SideMenuPanel

  page_url("http://puppies.herokuapp.com")

  #We are using the plural version of the declaration. In each case it will create a method
  #named “[name]_elements” where [name] is replaced by the name we gave in our declaration
  divs(:name, :class => 'name')
  buttons(:view_detail, :value => 'View Details')

  def select_puppy(name = 'Brook')
    index = index_for(name)
    button_element(:value => 'View Details', :index => index).click
  end

  private

  #In our case we are matching the text of the div passed in as the parameter with the name of the puppy. This will give us the index we need.
  def index_for(name)
    name_elements.find_index {|the_div| the_div.text == name}
  end

end