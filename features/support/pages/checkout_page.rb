require_relative 'error_panel' #include the error_panel field
require_relative 'side_menu_panel'

class CheckoutPage
  include PageObject
  include DataMagic
  include ErrorPanel #Include the module
  include SideMenuPanel

  #DEFAULT DATA using the faker gem in order to random the data
  DEFAULT_DATA = {
      'name' => Faker::Name.name,
      'address' => Faker::Address.street_address,
      'email' => Faker::Internet.email,
      'pay_type' => 'Purchase order'
  }

  text_field(:name, :id => "order_name")
  text_field(:address, :id => "order_address")
  text_field(:email, :id => "order_email")
  select_list(:pay_type, :id => "order_pay_type")
  button(:place_order, :value => "Place Order")

  def checkout(data)
    self.name = data['name']
    self.address = data['address']
    self.email = data['email']
    self.pay_type = data['pay_type']
    place_order
  end

  def checkout_with_default_data(data = {}) # data ={} This is a default parameter to a method
    populate_page_with DEFAULT_DATA.merge(data) #merges the data passed to the method with the default data of the page, this method takes a hash
    place_order
  end

  def checkout_with_default_data_02(data = {}) # data ={} This is a default parameter to a method
    data = DEFAULT_DATA.merge(data) #merges the data passed to the method with the default data of the page
    self.name = data['name']
    self.address = data['address']
    self.email = data['email']
    self.pay_type = data['pay_type']
    place_order
  end

  def checkout_using_data_magic(data = {})
    populate_page_with data_for(:checkout_page, data)
    place_order
  end

end