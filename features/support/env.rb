require 'rspec-expectations'
require 'page-object'
require 'faker'
require 'data_magic'

#Con la siguiente linea estoy agregando Page Factory a la clase world, de esta manera todas las clases pueden utilizarla
#Una manera de extender Cucumber es agregando metodos a la clase World
World(PageObject::PageFactory)

#When Cucumber starts it performs the following tasks:
#• Creates a class named World
#• Loads all of the files in the support directory
#• Adds all of the step definitions to the World class
#• Runs the Scenarios by executing the steps as methods on World

